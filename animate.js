		var done = false;

		$(document).ready(function () {
		    $(window).scroll(function () {
		        if ($('.pagetitle').isOnScreen()) {
		            // The element is visible, do something
		            if (!done) {
		                done = true;
		                animate();
		            }

		        } else {
		            // The element is NOT visible, do something else
		        }
		    });
		});

		$.fn.isOnScreen = function () {

		    var win = $(window);

		    var viewport = {
		        top: win.scrollTop(),
		        left: win.scrollLeft()
		    };
		    viewport.right = viewport.left + win.width();
		    viewport.bottom = viewport.top + win.height();

		    var bounds = this.offset();
		    bounds.right = bounds.left + this.outerWidth();
		    bounds.bottom = bounds.top + this.outerHeight();

		    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

		};

		function animate() {
		    $('.pagenumber').each(function () {
		        $(this).prop('Counter', 0).animate({
		            Counter: $(this).text()
		        }, {
		            duration: 2500,
		            easing: 'swing',
		            step: function (now) {
		                $(this).text(Math.ceil(now));
		            }
		        });
		    });
		}